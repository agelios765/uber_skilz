<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth:admin')->group(function () {
    Route::prefix('admins')->as('admin.')->namespace('Admin')->group(function () {
        Route::get('/', 'DashboardController@index')->name('dashboard');
        Route::post('logout', 'Auth\LoginController@logout')->name('logout');
    });
});

Route::get('admins/login', 'Admin\Auth\LoginController@showLoginForm')->middleware('guest:admin')->name('admin.login');
Route::post('admins/login', 'Admin\Auth\LoginController@login')->middleware('guest:admin')->name('admin.login');

Route::get('/', 'HomeController@index')->name('dashboard');
Route::get('benefits', 'BenefitsController@index')->name('benefits');

//Route::get('sign-up', 'Auth\LoginController@showLoginForm')->name('sign_up');

Route::get('sign-up', 'Auth\RegisterController@showRegistrationForm')->name('sign_up');
Route::post('register', 'Auth\RegisterController@register')->name('registration');

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

//Route::get('profile', 'UserController@profile')->middleware(['auth', 'verified'])->name('user.profile');
Route::get('connect', 'UserController@connect')->middleware(['auth', 'verified'])->name('user.connect');
Route::post('connect', 'UserController@applyConnect')->middleware(['auth', 'verified'])->name('user.connect');
Route::get('member', 'UserController@member')->middleware(['auth', 'verified'])->name('user.member');

Route::get('subcategory', 'UserController@getSubcategory')->middleware(['auth', 'verified'])->name('user.getSubcategory');

Route::get('success', 'PaymentController@successful');
Route::get('cancel', 'PaymentController@cancel');
Route::post('itn', 'PaymentController@itn');

Route::get('private-sector', 'UserController@privateSector')->middleware(['auth', 'verified'])->name('user.private_sector');
Route::post('private-sector', 'UserController@storePrivateSector')->middleware(['auth', 'verified'])->name('user.private_sector');
Route::get('goverment-sector', 'UserController@govermentSector')->middleware(['auth', 'verified'])->name('user.goverment_sector');
Route::post('goverment-sector', 'UserController@storeGovermentSector')->middleware(['auth', 'verified'])->name('user.goverment_sector');
Route::get('business-sector', 'UserController@businessSector')->middleware(['auth', 'verified'])->name('user.business_sector');
Route::post('business-sector', 'UserController@storeBusinessSector')->middleware(['auth', 'verified'])->name('user.business_sector');

Route::get('invite', 'UserController@invite')->middleware(['auth', 'verified'])->name('user.invite');

//Route::post('sign-up', 'LoginController@showLoginForm')->name('registration');
//Route::get('signgjh-up', 'LoginController@showLoginForm')->name('login');
Route::get('getConnected', 'GetConnectedController@index')->name('getConnected');

Route::get('purpose', 'PurposeController@index')->name('purpose');
Route::get('privacy', 'PrivacyController@index')->name('privacy');
Route::get('terms', 'TermsController@index')->name('terms');
