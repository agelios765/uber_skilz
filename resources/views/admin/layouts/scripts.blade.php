<script src="{{ asset('admin/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('admin/vendor/popper.js/popper.min.js') }}"></script>
<script src="{{ asset('admin/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('admin/vendor/chart.js/chart.min.js') }}"></script>
<script src="{{ asset('admin/js/carbon.js') }}"></script>
<script src="{{ asset('admin/js/demo.js') }}"></script>