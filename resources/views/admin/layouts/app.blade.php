<!doctype html>
<html lang="en">
<head>
    @include('admin.layouts.header')
</head>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">
    @include('admin.layouts.nav')

    <div class="main-container">
        @include('admin.layouts.sidebar')

        <div class="content">
            @yield('content')
        </div>
    </div>
</div>
@include('admin.layouts.scripts')
@yield('scripts')
</body>
</html>
