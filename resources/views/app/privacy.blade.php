@extends('app.layout.app')

@section('content')
    <section class="wrapper wrapper_benefits">
        @include('app.layout.header')
        @include('app.layout.mobile')
        <section class="privacy-page">
            <div class="privacy-page__inner">
                <h1 class="privacy-page__title">Privacy Policy</h1>
                <span class="privacy-page__text">We absolutely respect your privacy. We do not sell, trade or share personal information with organisations or individuals not connected with the Service. This section explains what information is stored and how it is used. It should be read in conjunction with our User and Member Agreements.</span>
                <span class="privacy-page__text">This Privacy Policy was last modified on 08 August 2018.</span>
                <div class="privacy-page__item privacy__item_first">
                    <h2 class="privacy-page__item__title">Information that we collect and store</h2>
                    <span class="privacy-page__item__text">We only collect, store or use information that we consider necessary for the purpose of providing the best service possible to all of our members and users. You are free to browse our website without providing any personal information to us. Customers (“Users”) who wish to find a Service Provider (“Member”) can download the App for free. We ask Members for further information including but not limited to their date of birth, gender, trade, specific skills, working preferences, other anecdotal biographical information. We store this information in the cloud. We also hold all information related to service reviews, including all descriptions and performance ratings. For quality control and training purposes, we may monitor or record your communication with our support team. In addition to the information referred to above, we also use third party tracking software provided by Google Analytics to monitor your use of your Website in order to enable us to further improve your use and the functionality of our Website. This software enables us to monitor actions such as your key presses, mouse movements, clicks etc. No sensitive information fields (such as credit card details) are recorded.</span>
                </div>
                <div class="privacy-page__item">
                    <h2 class="privacy-page__item__title">How we use this information</h2>
                    <span class="privacy-page__item__text">We check the accuracy of the information that you provide on registration and during your continued use of the Service and remind you of your obligations to provide accurate information to us as set out in our respective user and membership Agreements.</span>
                    <span class="privacy-page__item__text">As part of the process of verifying information provided by members we may carry out, either directly or via carefully selected and appropriately regulated third party data verification agencies, identity verification checks on you to verify the information you have provided to us. This may include cross-referencing the information you have provided with – by way of example – electoral roll data, passport data, credit record data etc. If you do not consent to such checks being carried out please do not register with us. None of the documents required to conduct these checks are stored or revealed to users of the Service.</span>
                    <span class="privacy-page__item__text">We may use the contact information you provide to contact you via email, fax, post or telephone regarding your registration, or any other matter that we consider relevant to your use of our Service. We make available the contact details submitted by the member, to enable users to initiate contact with members. Before a user is able to access the contact details of any Service member, we collect the contact details of the user in question first. This information is collected to enable us to assess the status and quality of the job and collect a review, and is not released to any party.  Any ratings and comments from users and members in relation to historic Projects (services’) are available for all users of the Website (and mobile application) to see. We may use any information we collect about you, including any information we may have obtained from your use of the Service, to resolve disputes, provide customer support, troubleshoot problems and enforce our user and /or member agreements and policies. We may look across multiple users and/or members to identify problems or resolve disputes. In the unlikely event that fees remain unpaid on the account of a member, we may pass your information to a credit recovery company.</span>
                    <span class="privacy-page__item__text">We may disclose your personal data if we are compelled to do so by law, or in response to a valid, legally compliant request by a law enforcement or governmental authority. We may also disclose aggregated demographic and profile data (in which neither you nor any other individual is identified) to our commercial and marketing partners.</span>
                </div>
                <div class="privacy-page__item">
                    <h2 class="privacy-page__item__title">Security measures</h2>
                    <span class="privacy-page__item__text">To prevent unauthorised access, maintain accuracy of our records and to ensure proper use and protection of Your Information, we have put appropriate physical, electronic and managerial procedures in place. Our server is protected by a dedicated firewall and we operate strict internal policies with regard to accessing personal information. However, we cannot guarantee the security of any information or data which is transmitted over public or third party networks.</span>
                </div>
                <div class="privacy-page__item">
                    <h2 class="privacy-page__item__title">Cookies</h2>
                    <span class="privacy-page__item__text">This website uses cookies to identify repeat visitors. We protect your privacy by only storing a simple ID number and token string that is only meaningful to this website. Your personal details, such as your name or phone number, are not stored in a cookie. You may set your computer browser to reject cookies but this may preclude your use of certain parts of the Service. Third party vendors, including Google, may show UberSkillz and/or Johnny Rocket ads on sites on the internet and use cookies to serve ads based on a user’s prior visits to this website. Users may opt out of Google’s use of cookies by visiting the Google advertising opt-out page.</span>
                </div>
                <div class="privacy-page__item">
                    <h2 class="privacy-page__item__title">Retention of your information</h2>
                    <span class="privacy-page__item__text">We will retain your personal information only as long as is necessary for the purposes to which you consent under this Privacy Policy, after which we will delete it from our systems.</span>
                </div>
                <div class="privacy-page__item">
                    <h2 class="privacy-page__item__title">Access to information</h2>
                    <span class="privacy-page__item__text">Please contact us if you would like to know what information we are storing about you. We may ask for proof of identity before providing you with any information requested and reserve the right to refuse to provide information if identity is not established. Any changes to this policy will be posted on this Website. If you have any further comments or questions regarding our user and/or member agreements, then please contact us.</span>
                </div>
            </div>
        </section>
        @include('app.layout.footer')
    </section>
@endsection