@extends('app.layout.app')

@section('content')
    <section class="wrapper">
        @include('app.layout.header')
        @include('app.layout.mobile')
        <section class="head">
            <div id="gp_particles_1" class="gp-particles"></div>
            <div class="head__inner">
                <h1 class="head__title">Connecting Skills with Opportunities</h1>
                <span class="head__desc">A digital network of the best skills and expertise.</span>
                <div class="head__btn">
                    <a href="{{ route('sign_up') }}" class="btn"><span class="btn__text">Get Connected</span></a>
                    <a href="{{ route('benefits') }}" class="btn btn_blue"><span class="btn__text btn__text_white">Benefits to Join</span></a>
                </div>
            </div>
        </section>
        <section class="difference">
            <div class="difference__inner">
                <div class="difference__circle">
                    <div class="circle"></div>
                </div>
                <div class="difference__desc">
                    <h2 class="difference__desc__title">Make a difference</h2>
                    <h2 class="difference__desc__sub-title">Together we can change the future</h2>
                    <span class="difference__desc__text">UberSkillz is the largest skills network aggregator, which enables its members to make their expertise available and visible to connect with others seeking these specific skills. Our innovative SkillzFinder geolocation app technology enables customers to contact and converse with you directly regarding their needs. We invite you to become an UberSkiller today and help change the future of our economy.</span>
                    <div class="difference__desc__btn">
                        <a class="btn" href=""><span class="btn__text">SEE WHAT SETS US APART</span></a>
                    </div>
                </div>
            </div>
        </section>
        @include('app.layout.footer')
    </section>
@endsection