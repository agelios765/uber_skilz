@extends('app.layout.app')

@section('content')
    <section class="wrapper wrapper_sign-up">
        @include('app.layout.header')
        @include('app.layout.mobile')
        <section class="form-area">
            <div class="form-area__form">
                <h2 class="form-area__title">Get connected</h2>
                <h2 class="form-area__title_big">Wellcome Skiller</h2>

                <div class="form-area__form form-area__form_top">
                    <div class="form-group">
                        <span class="form-area__text">The last step to get you connected and earning!</span>
                        <span class="form-area__text">Download your free App, sign in and activate to become instantly
visible on the geolocation App.</span>
                        <span class="form-area__text">People searching for you and who need your skills and services
will find you on their phone and will connect with you. </span>
                        <span class="form-area__text">Simple, easy and convenient</span>
                        <a href="" class="form-btn form-btn_d"><span class="form-btn__text">COMING SOON </span></a>
                    </div>
                    <div class="steps-group">
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle circle_head circle_head_first active">1</div>
                            </div>
                            <div class="text text_head current-head">Signup to join</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle active"></div>
                            </div>
                            <div class="text current-step active">Personal information</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle active"></div>
                            </div>
                            <div class="text active">Verify your email</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle circle_head active">2</div>
                            </div>
                            <div class="text text_head active">Become an UberSkiller</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle active"></div>
                            </div>
                            <div class="text active">Select membership</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle active"></div>
                            </div>
                            <div class="text active">Member information</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle active"></div>
                            </div>
                            <div class="text active">Membership payment</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle circle_head active">3</div>
                            </div>
                            <div class="text text_head active">Connect</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle active"></div>
                            </div>
                            <div class="text active">Download app</div>
                        </div>
                    </div>

                </div>
            </div>

        </section>
        @include('app.layout.footer')
    </section>
@endsection