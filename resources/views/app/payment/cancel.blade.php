@extends('app.layout.app')

@section('content')
    <section class="wrapper wrapper_sign-up">
        @include('app.layout.header')
        @include('app.layout.mobile')
        <section class="form-area">
            <div class="form-area__form">
                <h2 class="form-area__title">Payment unsuccessful</h2>
                <h2 class="form-area__title_big">Contact us</h2>

                <div class="form-area__form form-area__form_top">
                    <div class="form-group">
                        <span class="form-area__text">Oops! Something went wrong during the payment process.</span>
                        <span class="form-area__text">No worries, your membership profile is still loaded and will be
activated on receipt of payment confirmation. Please email our
support team for more assistance. </span>
                        <a href="mailto:test@test.test" class="form-btn"><span class="form-btn__text">EMAIL US </span></a>
                    </div>
                    <div class="steps-group">
                        <div class="steps-group">
                            <div class="steps-group__item">
                                <div class="circle-wrapp">
                                    <div class="circle circle_head circle_head_first active">1</div>
                                </div>
                                <div class="text text_head current-head">Signup to join</div>
                            </div>
                            <div class="steps-group__item">
                                <div class="circle-wrapp">
                                    <div class="circle active"></div>
                                </div>
                                <div class="text current-step active">Personal information</div>
                            </div>
                            <div class="steps-group__item">
                                <div class="circle-wrapp">
                                    <div class="circle"></div>
                                </div>
                                <div class="text">Verify your email</div>
                            </div>
                            <div class="steps-group__item">
                                <div class="circle-wrapp">
                                    <div class="circle circle_head">2</div>
                                </div>
                                <div class="text text_head">Become an UberSkiller</div>
                            </div>
                            <div class="steps-group__item">
                                <div class="circle-wrapp">
                                    <div class="circle"></div>
                                </div>
                                <div class="text">Select membership</div>
                            </div>
                            <div class="steps-group__item">
                                <div class="circle-wrapp">
                                    <div class="circle"></div>
                                </div>
                                <div class="text">Membership payment</div>
                            </div>
                            <div class="steps-group__item">
                                <div class="circle-wrapp">
                                    <div class="circle circle_head">3</div>
                                </div>
                                <div class="text text_head">Create your profile</div>
                            </div>
                            <div class="steps-group__item">
                                <div class="circle-wrapp">
                                    <div class="circle"></div>
                                </div>
                                <div class="text">Member information</div>
                            </div>
                            <div class="steps-group__item">
                                <div class="circle-wrapp">
                                    <div class="circle circle_head">4</div>
                                </div>
                                <div class="text text_head">Connect</div>
                            </div>
                            <div class="steps-group__item">
                                <div class="circle-wrapp">
                                    <div class="circle"></div>
                                </div>
                                <div class="text">Invite friends</div>
                            </div>
                            <div class="steps-group__item">
                                <div class="circle-wrapp">
                                    <div class="circle"></div>
                                </div>
                                <div class="text">Download app</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @include('app.layout.footer')
    </section>
@endsection