@extends('app.layout.app')

@section('content')
    <section class="wrapper wrapper_getconnected">
        @include('app.layout.header')
        @include('app.layout.mobile')
        <section class="get-connected">
            <div class="get-connected__inner">
                <div class="get-connected__desc">
                    <h2 class="get-connected__desc__title">Get connected</h2>
                    <h3 class="get-connected__desc__title-big">Make a difference</h3>
                    <span class="get-connected__desc__text">UberSkillz is a platform for skilled people irrespective of age, race, creed or gender, to become discoverable by publishing their skills to fill the gaps in the economy.</span>
                    <ul class="get-connected__desc__list">
                        <li class="get-connected__desc__list__item">Network and collaborate with each other</li>
                        <li class="get-connected__desc__list__item">Become available to whomever in need of your skill
                        </li>
                        <li class="get-connected__desc__list__item">Share skills for employment or mentorship purposes
                        </li>
                    </ul>
                </div>
                <div class="get-connected__circle">
                    <div class="circle"></div>
                </div>
            </div>
        </section>
        <section class="how">
            <div class="how__inner">
                <h2 class="how__title">How UberSkillz Works</h2>
                <span class="how__text">Three easy steps to sell your skills by making them visible on our geolocation App for customers to start contacting you:</span>
                <div class="how__board">
                    <div class="how__board__item">
                        <div class="circle"><span class="number">1</span></div>
                        <h3 class="how__board__title">Signup to join</h3>
                        <span class="how__board__text">Click, subscribe to our principles and register.</span>
                        <ul class="how__board__list">
                            <li class="how__board__list__item">Sell your skills by making them visible to others</li>
                            <li class="how__board__list__item">Easy find-me function on our free App</li>
                            <li class="how__board__list__item">Super cheap membership</li>
                        </ul>
                    </div>
                    <div class="how__board__item">
                        <div class="circle"><span class="number">2</span></div>
                        <h3 class="how__board__title">Create your profile</h3>
                        <span class="how__board__text">Click to start the process.</span>
                        <ul class="how__board__list">
                            <li class="how__board__list__item">Build your own easy-to-use, unique profile</li>
                            <li class="how__board__list__item">Organised and orderly skills category platform</li>
                            <li class="how__board__list__item">Boost your profile for better reach</li>
                        </ul>
                    </div>
                    <div class="how__board__item">
                        <div class="circle"><span class="number">3</span></div>
                        <h3 class="how__board__title">Connect</h3>
                        <span class="how__board__text">Click and download the App.</span>
                        <ul class="how__board__list">
                            <li class="how__board__list__item">Cool geo-location connectivity</li>
                            <li class="how__board__list__item">Easy to use and navigate</li>
                            <li class="how__board__list__item">Chat capability</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="costs">
            <div class="costs__inner">
                <h3 class="costs__title">What it costs</h3>
                <span class="costs__text">R100 – Once-off admin fee</span>
                <span class="costs__text">R500 – Membership Profile published in one skill category</span>
                <span class="costs__text">R350 – 12-month Membership fee</span>

                <div class="costs__desc">
                    <h4 class="costs__desc__title">Total: R 950</h4>
                    <span class="costs__desc__text">Please note: Earn your R350 Membership fee back by successfully introducing 2 or more friends; discount rates apply if you would like to publish in more than one skill category – email us your preferences for fee estimation.</span>
                </div>
                <div class="costs__desc__btn"><a href="{{ route('sign_up') }}" class="btn"><span class="btn__text">SIGNUP TO CONNECT</span></a>
                </div>
            </div>
        </section>
        <section class="faq" id="faq">
            <div class="faq__inner">
                <h2 class="faq__title">FAQs</h2>
                <span class="faq__text">Some information that our support team tell new members everyday.</span>
                <div class="drop-wrapper">
                    <div class="drop">
                        <div class="drop__head js-drop">
                            <h3 class="drop__title"><span class="arrow js-arrow"></span>Register as an UberSkiller</h3>
                        </div>
                        <div class="drop__bottom">
                            <span class="drop__text">Use the unique membership number sent to you by the member who introduced you to UberSkillz, login, create your own unique password and follow the 3 easy steps to become an UberSkillz in no time!</span>
                        </div>
                    </div>
                    <div class="drop">
                        <div class="drop__head js-drop">
                            <h3 class="drop__title"><span class="arrow js-arrow"></span>Introduce a new UberSkiller</h3>
                        </div>
                        <div class="drop__bottom">
                            <span class="drop__text">UberSkillz issues every registered member with a unique membership code, which can then be forwarded to the member’s contacts. This is an invitation to join UberSkillz and will allow these persons to also become members.</span>
                        </div>
                    </div>
                    <div class="drop">
                        <div class="drop__head js-drop">
                            <h3 class="drop__title"><span class="arrow js-arrow"></span>Claim your reward</h3>
                        </div>
                        <div class="drop__bottom">
                            <span class="drop__text">As soon as you have registered as a member, we will send you a notification with your unique membership number. By forwarding this number to your contacts who you think might also benefit from our membership, and two or more of them also become members, we will refund you your entire annual membership fee of R350. How cool is that?</span>
                        </div>
                    </div>
                    <div class="drop">
                        <div class="drop__head js-drop">
                            <h3 class="drop__title"><span class="arrow js-arrow"></span>Upload your CV / Profile</h3>
                        </div>
                        <div class="drop__bottom">
                            <span class="drop__text">Your CV / Profile will be hosted on your Member Profile Page, enabling prospective employers to contact you directly. It can be uploaded either at registration, or on the UberSkillz App registration, or later on your member profile page and must not be larger than 3.5MB in .pdf format.</span>
                        </div>
                    </div>
                    <div class="drop">
                        <div class="drop__head js-drop">
                            <h3 class="drop__title"><span class="arrow js-arrow"></span>Edit Profile</h3>
                        </div>
                        <div class="drop__bottom">
                            <span class="drop__text">Members can edit their profile by clicking the edit tab on the Member Profile Page after logging in. You can change personal details, profile information and even your skills category. So if one category doesn’t work for you, try another one!</span>
                        </div>
                    </div>
                    <div class="drop">
                        <div class="drop__head js-drop">
                            <h3 class="drop__title"><span class="arrow js-arrow"></span>Upload your profile image</h3>
                        </div>
                        <div class="drop__bottom">
                            <span class="drop__text">Simply upload your image or company logo on registration or later on your Member Profile Page. The image will display on the UBERSKILLZ App and on the user’s SkillzFinder App, so that people can easily identify you when searching.</span>
                        </div>
                    </div>
                    <div class="drop">
                        <div class="drop__head js-drop">
                            <h3 class="drop__title"><span class="arrow js-arrow"></span>Social links</h3>
                        </div>
                        <div class="drop__bottom">
                            <span class="drop__text">Simply link your social media platforms on the Edit tab on your Member Profile Page.</span>
                        </div>
                    </div>
                    <div class="drop">
                        <div class="drop__head js-drop">
                            <h3 class="drop__title"><span class="arrow js-arrow"></span>LiveChat tool</h3>
                        </div>
                        <div class="drop__bottom">
                            <span class="drop__text">This functionality works just like WhatsApp and enables live communication between UberSkillz members on the website platform. You can communicate with members individually or after setting up a group or forum.</span>
                        </div>
                    </div>
                    <div class="drop">
                        <div class="drop__head js-drop">
                            <h3 class="drop__title"><span class="arrow js-arrow"></span>UberSkillz App activation</h3>
                        </div>
                        <div class="drop__bottom">
                            <span class="drop__text">By downloading and activating the membership UberSkillz App, members will immediately be visible on user’s geolocation SkillzFinder App. It is advisable to always keep your UberSkillz App on so that people can easily find and connect with you in their search area.</span>
                        </div>
                    </div>
                    <div class="drop">
                        <div class="drop__head js-drop">
                            <h3 class="drop__title"><span class="arrow js-arrow"></span>How to boost your App profile
                                status</h3>
                        </div>
                        <div class="drop__bottom">
                            <span class="drop__text">This functionality enables members to be more visible than other members for users searching skills on their SkillzFinder App. Login to your UberSkillz App, click on the ‘Boost-Me-Up’ tab on your Profile Page and simply buy $tarbuck$ credits in ZAR to enhance your normal profile to Uber Profile status. Your profile icon will then start glowing compared to other member’s static icons. Outshine the competition and get more business!</span>
                        </div>
                    </div>
                    <div class="drop">
                        <div class="drop__head js-drop">
                            <h3 class="drop__title"><span class="arrow js-arrow"></span>Anything we've missed</h3>
                        </div>
                        <div class="drop__bottom">
                            <span class="drop__text">Drop us a request at <a class="link"
                                                                             href="mailto:info@uberskillz.com">info@uberskillz.com</a> and we will reply asap. Any good idea is welcome and our rocket scientists are on standby to implement it!</span>
                        </div>
                    </div>
                    <div class="drop">
                        <div class="drop__head js-drop">
                            <h3 class="drop__title"><span class="arrow js-arrow"></span>Free downloads and links</h3>
                        </div>
                        <div class="drop__bottom">
                            <span class="drop__text">Click the ‘Links’ tab on the website for more information on small business registration, administration and setup procedures; as well as links to SARS and CPIC pages pertaining small businesses segmentation and registration.</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="trusts" id="links">
            <div class="trusts__inner">
                <h2 class="trusts__title">Click on the following images for small businesses information:</h2>
                <div class="trusts-item-wrapper">
                    <div class="trusts-item">
                        <h3 class="trusts-item__title">Small Business Information</h3>
                        <a href="http://www.sars.gov.za/ClientSegments/Businesses/SmallBusinesses/Pages/default.aspx"
                           target="_blank" class="trusts-item__link">
                            <img src="{{ asset('images/trusts/stars.png') }}" alt="">
                        </a>
                    </div>
                    <div class="trusts-item">
                        <h3 class="trusts-item__title">Register Your Business</h3>
                        <a href="http://www.cipc.co.za/index.php/register-your-business/you-decide/" target="_blank"
                           class="trusts-item__link">
                            <img src="{{ asset('images/trusts/mem.png') }}" alt="">

                        </a>
                    </div>
                    <div class="trusts-item">
                        <h3 class="trusts-item__title">Business Helpdesk</h3>
                        <a href="https://webranding.co.za/wp-content/uploads/2018/08/UHelpDesk.pdf" target="_blank"
                           class="trusts-item__link">
                            <img src="{{ asset('images/trusts/uname.png') }}" alt="">

                        </a>
                    </div>
                </div>
            </div>
        </section>
        @include('app.layout.footer')
    </section>
@endsection