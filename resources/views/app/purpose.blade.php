@extends('app.layout.app')

@section('content')
    <section class="wrapper wrapper_purpose">
        @include('app.layout.header')
        @include('app.layout.mobile')
        <section class="our-story">
            <div class="our-story__inner">
                <h3 class="our-story__title">Our story</h3>
                <h2 class="our-story__question">Why UberSkillz?</h2>
                <span class="our-story__text">The idea was simple: imagine if people who need your skills could find you on their phone?</span>
            </div>
        </section>
        <section class="we-make">
            <div class="we-make__inner">
                <span class="we-make__text">We make your expertise and work experience visible to others and effortlessly connect you with those who need them: easily, quickly and cheaply. UberSkillz is a place where people can advertise their skills, find work and get paid. It is also a place where anyone can find people to do any job they need done.</span>
            </div>
        </section>
        <section class="objectives">
            <div class="objectives__inner">
                <h2 class="objectives__title">Aims and objectives</h2>
                <span class="objectives__text">South Africa has a massive pool of highly skilled people who (are lying idle) have been side-lined, retrenched, pensioned off or who have otherwise lost their source of income or who do not comply with current legislation. In short, taken out of the game. On the other hand, the country is in dire need of these skills, whether in the public or private sector. UberSkillz will attempt to solve this problem.</span>
            </div>
        </section>
        <section class="target">
            <div class="target__inner">
                <div class="target__item">
                    <h2 class="target__title">Vision</h2>
                    <span class="target__text">To be the largest global digital skills aggregator platform for people who possess specific, critical, scarce or lost skills and who are willing to contribute and make a difference for themselves and to the economy of their country.</span>
                </div>
                <div class="target__item">
                    <h2 class="target__title">Mission</h2>
                    <span class="target__text">To provide a reputable network which mobilises people to make their expertise and work experience visible and available, enabling them to connect and to communicate directly with others seeking their specific skills.</span>
                </div>
            </div>
        </section>
        <section class="influencer">
            <div class="influencer__inner">
                <span class="influencer__text">UberSkillz is an influencer, offering an effective skills network for the old and the new generation, to launch themselves anywhere in the world. These are the very people who will be providing the millions of jobs required to reduce the global unemployment rate.</span>
                <span class="influencer__text">UberSkillz promotes unity of purpose through good work ethics, responsibility, accountability, integrity, honesty, respect and punctuality to connect like-minded people.</span>
            </div>
        </section>
        @include('app.layout.footer')
    </section>
@endsection