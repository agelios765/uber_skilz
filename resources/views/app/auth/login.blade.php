@extends('app.layout.app')

@section('content')
    <section class="wrapper wrapper_sign-up">
        @include('app.layout.header')
        @include('app.layout.mobile')
        <section class="form-area">
            <h2 class="form-area__title">Get connected</h2>
            <h2 class="form-area__title_big">Signup to Join</h2>
            <span class="form-area__text">Complete 4 easy steps to activate your membership! </span>
            <div class="form-area__form form-area__form_top">
                <div class="form-group">
                    <form method="post" action="{{ route('registration') }}">
                        {{ csrf_field() }}
                        <div class="double-input">
                            <div class="input-wrapp">
                                <label for="nameEdit">Ваше имя:</label>
                                <input id="nameEdit" class="{{ $errors->has('name') ? ' error' : '' }}" name="name" type="text">
                            </div>
                            <div class="input-wrapp">
                                <label for="lastNmae">Last name (required):</label>
                                <input id="lastNmae" class="{{ $errors->has('lastname') ? ' error' : '' }}" name="lastname" type="text">
                            </div>
                        </div>

                        <div class="input-wrapp">
                            <label for="number">Mobile number</label>
                            <input id="number" name="number" class="{{ $errors->has('number') ? ' error' : '' }}" type="text">
                        </div>
                        <div class="input-wrapp">
                            <label for="email">Email</label>
                            <input id="email" class="{{ $errors->has('email') ? ' error' : '' }}" name="email" type="text">
                        </div>
                        <div class="input-wrapp">
                            <label for="password">Password</label>
                            <input id="password" class="{{ $errors->has('password') ? ' error' : '' }}" name="password" type="password">
                        </div>
                        <div class="input-wrapp">
                            <label for="confirmPassword">Confirm password</label>
                            <input id="confirmPassword" class="{{ $errors->has('password') ? ' error' : '' }}" name="password_confirmation" value="" type="password">
                        </div>
                    <button type="submit" class="form-btn"><span class="form-btn__text">NEXT</span></button>
                    </form>
                </div>

                <div class="steps-group">
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle circle_head circle_head_first active">1</div>
                        </div>
                        <div class="text text_head current-head">Signup to join</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle active"></div>
                        </div>
                        <div class="text current-step active">Personal information</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle"></div>
                        </div>
                        <div class="text">Verify your email</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle circle_head">2</div>
                        </div>
                        <div class="text text_head">Become an UberSkiller</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle"></div>
                        </div>
                        <div class="text">Select membership</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle"></div>
                        </div>
                        <div class="text">Member information</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle"></div>
                        </div>
                        <div class="text">Membership payment</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle circle_head">3</div>
                        </div>
                        <div class="text text_head">Connect</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle"></div>
                        </div>
                        <div class="text">Download app</div>
                    </div>
                </div>

            </div>
        </section>

        @include('app.layout.footer')
    </section>
@endsection