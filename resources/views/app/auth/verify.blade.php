@extends('app.layout.app')

@section('content')
    <section class="wrapper wrapper_sign-up">
        @include('app.layout.header')
        @include('app.layout.mobile')
        <section class="form-area">
            <h2 class="form-area__title">Get connected</h2>
            <h2 class="form-area__title_big">Verify your email</h2>
            <div class="form-area__form form-area__form_top">
                <div class="form-group">
                    <div class="form-area__form">
                        <span class="form-area__text">Please verify your email address to activate your membership. </span>
                        <span class="form-area__text">By clicking ‘Verify Email’, you hereby agree to the  <a
                                    href="{{ route('privacy') }}" target="_blank" class="form-area__text__bold">Privacy Policy</a>
                            and <a href="{{ route('terms') }}" target="_blank"
                                   class="form-area__text__bold">Terms and Conditions</a> of SkillzTrader.</span>
                        <a href="{{ route('verification.resend') }}" class="form-btn-link">Resend email</a>
                    </div>
                </div>
                <div class="steps-group">
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle circle_head circle_head_first active">1</div>
                        </div>
                        <div class="text text_head current-head">Signup to join</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle active"></div>
                        </div>
                        <div class="text current-step active">Personal information</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle active"></div>
                        </div>
                        <div class="text active">Verify your email</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle circle_head">2</div>
                        </div>
                        <div class="text text_head">Become an UberSkiller</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle"></div>
                        </div>
                        <div class="text">Select membership</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle"></div>
                        </div>
                        <div class="text">Member information</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle"></div>
                        </div>
                        <div class="text">Membership payment</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle circle_head">3</div>
                        </div>
                        <div class="text text_head">Connect</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle"></div>
                        </div>
                        <div class="text">Download app</div>
                    </div>
                </div>
            </div>
        </section>
        @include('app.layout.footer')
    </section>
@endsection