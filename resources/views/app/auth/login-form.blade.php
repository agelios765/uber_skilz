@extends('app.layout.app')

@section('content')
    <section class="wrapper wrapper_benefits">
        @include('app.layout.header')
        @include('app.layout.mobile')
        {{--        {{ dd(session()->all()) }}--}}
        <section class="form-area form-area_log-in">
            <img width="517" height="163" src="images/forms/ubrs.png" alt="">
            <h2 class="form-area__title">Sign In</h2>
            <form method="post" action="{{ route('login') }}">
                @csrf
                <div class="input-wrapp">
                    <label for="email">Email</label>
                    <input id="email" name="email" type="text">
                </div>
                <div class="input-wrapp">
                    <label for="password">Password</label>
                    <input id="password" name="password" type="password">
                </div>
                <button type="submit" class="form-btn"><span class="form-btn__text">SIGN IN</span></button>
            </form>
        </section>
        @include('app.layout.footer')
    </section>
@endsection