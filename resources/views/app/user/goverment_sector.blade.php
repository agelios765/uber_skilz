@extends('app.layout.app')

@section('content')
    <section class="wrapper wrapper_sign-up">
        @include('app.layout.header')
        @include('app.layout.mobile')
        <section class="form-area">
            <h2 class="form-area__title">Get connected</h2>
            <h2 class="form-area__title_big">Create your profile</h2>

            <div class="form-area__form form-area__form_top">
                <div class="form-group">
                    <span class="form-area__text">Please complete all the fields.</span>
                    <h2 class="form-area__title form-area__title_underline">Individual – Goverment Sector</h2>
                    <form method="post" action="{{ route('user.goverment_sector') }}">
                        @csrf
                        <div class="radiobtn-area">
                            <div class="radiobtn-area__text">Please select</div>
                            <div class="radiobtn-area__items">
                                <div class="radiobtn-area__item">
                                    <input type="radio" id="local" name="sector" value="1">
                                    <label for="local"><span>Local</span></label>
                                </div>
                                <div class="radiobtn-area__item">
                                    <input type="radio" id="provincial" name="sector" value="2">
                                    <label for="provincial"><span>Provincial</span></label>
                                </div>
                                <div class="radiobtn-area__item">
                                    <input type="radio" id="national" name="sector" value="3">
                                    <label for="national"><span>National</span></label>
                                </div>
                                <div class="radiobtn-area__item">
                                    <input type="radio" id="soe" name="sector" value="4">
                                    <label for="soe"><span>SOE</span></label>
                                </div>
                            </div>
                        </div>

                        <div class="input-wrapp input-wrapp_horisontal no-margin">
                            <label for="town">Location</label>
                            <input class="no-border-bottom" id="town" name="country" value="" placeholder="Dropdown - Town / City"
                                   type="text">
                        </div>
                        <div class="input-wrapp input-wrapp_horisontal">
                            <label for="province"></label>
                            <input id="province" value="" name="city" placeholder="Dropdown - Province / State" type="text">
                        </div>
                        <div class="input-wrapp input-wrapp_horisontal">
                            <label for="birth">Date of birth</label>
                            <input id="birth" value="" name="birthday" placeholder="DD/MM/YY" type="text">
                        </div>
                        <div class="input-wrapp input-wrapp_horisontal">
                            <label for="years">Years experience</label>
                            <input id="years" value="" name="experience" placeholder="Dropdown - Province / State" type="text">
                        </div>
                        <div class="input-wrapp input-wrapp_horisontal">
                            <label for="department">Department</label>
                            <select id="department" name="department">
                                <option value="">Select</option>
                                @foreach($departments as $i => $department)
                                    <option value="{{ $i }}">{{ $department }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input-wrapp input-wrapp_horisontal">
                            <label for="occupational">Occupational level</label>
                            <select id="occupational" name="occupation">
                                <option value="">Select</option>
                                @foreach($occupational as $i => $item)
                                    <option value="{{ $i }}">{{ $item }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="form-btn"><span class="form-btn__text">NEXT</span></button>
                        <a href="{{ route('user.connect') }}" class="form-btn"><span class="form-btn__text">BACK</span></a>
                    </form>
                </div>
                <div class="steps-group">
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle circle_head circle_head_first active">1</div>
                        </div>
                        <div class="text text_head current-head">Signup to join</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle active"></div>
                        </div>
                        <div class="text current-step active">Personal information</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle active"></div>
                        </div>
                        <div class="text active">Verify your email</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle circle_head active">2</div>
                        </div>
                        <div class="text text_head active">Become an UberSkiller</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle active"></div>
                        </div>
                        <div class="text active">Select membership</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle active"></div>
                        </div>
                        <div class="text active">Member information</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle"></div>
                        </div>
                        <div class="text">Membership payment</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle circle_head">3</div>
                        </div>
                        <div class="text text_head">Connect</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle"></div>
                        </div>
                        <div class="text">Download app</div>
                    </div>
                </div>

            </div>
        </section>
        @include('app.layout.footer')
    </section>
@endsection