@extends('app.layout.app')

@section('content')
    <section class="wrapper wrapper_sign-up">
        @include('app.layout.header')
        @include('app.layout.mobile')

        <section class="form-area">
            <div class="form-area__form">
                <h2 class="form-area__title">Get connected</h2>
                <h2 class="form-area__title_big">Become a Skiller</h2>
                <div class="form-area__form form-area__form_top">
                    <div class="form-group">
                        <span class="form-area__text">Please select your type of membership required. </span>
                        <form method="post" action="{{ route('user.connect') }}">
                            @csrf
                            <div class="radiobtn-area">
                                <div class="radiobtn-area__text">Membership</div>
                                <div class="radiobtn-area__items">
                                    <div class="radiobtn-area__item">
                                        <input type="radio" id="privateSector" name="gender" value="1">
                                        <label for="privateSector"><span>Individual – Private Sector</span></label>
                                    </div>
                                    <div class="radiobtn-area__item">
                                        <input type="radio" id="government" name="gender" value="2">
                                        <label for="government"><span>Individual – Government Sector</span></label>
                                    </div>
                                    <div class="radiobtn-area__item">
                                        <input type="radio" id="sme" name="gender" value="3">
                                        <label for="sme"><span>Business Sector (SME)</span></label>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="form-btn"><span class="form-btn__text">NEXT</span></button>
                        </form>
                    </div>
                    <div class="steps-group">
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle circle_head circle_head_first active">1</div>
                            </div>
                            <div class="text text_head current-head">Signup to join</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle active"></div>
                            </div>
                            <div class="text current-step active">Personal information</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle active"></div>
                            </div>
                            <div class="text active">Verify your email</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle circle_head active">2</div>
                            </div>
                            <div class="text text_head active">Become an UberSkiller</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle active"></div>
                            </div>
                            <div class="text active">Select membership</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle"></div>
                            </div>
                            <div class="text">Member information</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle"></div>
                            </div>
                            <div class="text">Membership payment</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle circle_head">3</div>
                            </div>
                            <div class="text text_head">Connect</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle"></div>
                            </div>
                            <div class="text">Download app</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        @include('app.layout.footer')
    </section>
@endsection