@extends('app.layout.app')

@section('content')
    <section class="wrapper wrapper_sign-up">
        @include('app.layout.header')
        @include('app.layout.mobile')
        <section class="form-area">
            <div class="form-area__form">
                <h2 class="form-area__title">Get connected</h2>
                <h2 class="form-area__title_big">Your referral code</h2>



                <div class="form-area__form form-area__form_top">
                    <div class="form-group">
                        <div class="input-wrapp input-wrapp_horisontal">
                            <p id="code" type="text">{{ md5('f') }}</p>
                        </div>
                        <span class="form-area__text">Invite your friends to sign up with this unique Referral Code.</span>
                        <a href="{{ route('user.download') }}" class="form-btn"><span class="form-btn__text">NEXT</span></a>
                    </div>
                    <div class="steps-group">
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle circle_head circle_head_first active">1</div>
                            </div>
                            <div class="text text_head current-head">Signup to join</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle active"></div>
                            </div>
                            <div class="text current-step active">Personal information</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle active"></div>
                            </div>
                            <div class="text active">Verify your email</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle circle_head active">2</div>
                            </div>
                            <div class="text text_head active">Become an UberSkiller</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle active"></div>
                            </div>
                            <div class="text active">Select membership</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle active"></div>
                            </div>
                            <div class="text active">Membership payment</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle circle_head active">3</div>
                            </div>
                            <div class="text text_head active">Create your profile</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle active"></div>
                            </div>
                            <div class="text active">Member information</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle circle_head active">4</div>
                            </div>
                            <div class="text text_head active">Connect</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle active"></div>
                            </div>
                            <div class="text active">Invite friends</div>
                        </div>
                        <div class="steps-group__item">
                            <div class="circle-wrapp">
                                <div class="circle"></div>
                            </div>
                            <div class="text">Download app</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @include('app.layout.footer')
    </section>
@endsection