@extends('app.layout.app')

@section('content')
    <section class="wrapper wrapper_benefits">
        @include('app.layout.header')
        @include('app.layout.mobile')

        <section class="form-area">
            <div class="form-area__form">
                <h2 class="form-area__title">Get connected</h2>
                <h2 class="form-area__title_big">Membership Payment</h2>

            </div>
            <div class="form-area__form form-area__form_top">
                <div class="form-group">
                    <span class="form-area__text">Super cheap annual membership!</span>
                    <span class="form-area__text">R100 – Once-off admin fee</span>
                    <span class="form-area__text">R500 – Membership Profile published in one skill category</span>
                    <span class="form-area__text">R350 – 12-month Membership fee</span>
                    <span class="form-area__text__big">Total: R 950</span>
                    {!! $form !!}

                    <span class="form-area__text form-area__text_list">Earn your R350 Membership Fee back by successfully
 introducing 2 or more friends.</span>
                    <span class="form-area__text form-area__text_list">Discount rates apply if you would like to feature in more than
 one skill category</span>
                    <span class="form-area__text form-area__text_list">Email us your preferences for a fee estimation.</span>
                </div>
                <div class="steps-group">
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle circle_head circle_head_first active">1</div>
                        </div>
                        <div class="text text_head current-head">Signup to join</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle active"></div>
                        </div>
                        <div class="text current-step active">Personal information</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle active"></div>
                        </div>
                        <div class="text active">Verify your email</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle circle_head active">2</div>
                        </div>
                        <div class="text text_head active">Become an UberSkiller</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle active"></div>
                        </div>
                        <div class="text active">Select membership</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle active"></div>
                        </div>
                        <div class="text active">Member information</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle active"></div>
                        </div>
                        <div class="text active">Membership payment</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle circle_head">3</div>
                        </div>
                        <div class="text text_head">Connect</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle"></div>
                        </div>
                        <div class="text">Download app</div>
                    </div>
                </div>
            </div>
        </section>

        @include('app.layout.footer')
    </section>
@endsection