@extends('app.layout.app')

@section('content')
    <section class="wrapper wrapper_benefits">
        @include('app.layout.header')
        @include('app.layout.mobile')
        <section class="form-area">
            <div class="form-area__form">
                <h2 class="form-area__title">Get connected</h2>
                <h2 class="form-area__title_big">Create your profile</h2>
            </div>
            <div class="form-area__form form-area__form_top">
                <div class="form-group">
                <span class="form-area__text">Please complete all the required fields.</span>
                <h2 class="form-area__title form-area__title_inderline">Get connected</h2>
                <form method="POST" action="send.php">
                    <div class="radiobtn-area">
                        <div class="radiobtn-area__text">Education</div>
                        <div class="radiobtn-area__items">
                            <div class="radiobtn-area__item">
                                <input type="radio" id="secondary" name="Secondary" value="">
                                <label for="secondary"><span>Secondary</span></label>
                            </div>
                            <div class="radiobtn-area__item">
                                <input type="radio" id="tertiary" name="tertiary" value="">
                                <label for="tertiary"><span>Tertiary</span></label>
                            </div>
                        </div>
                    </div>

                    <div class="input-wrapp input-wrapp_horisontal no-margin">
                        <label for="town">Location</label>
                        <input class="no-border-bottom" id="town" value="" placeholder="Dropdown - Town / City" type="text">
                    </div>
                    <div class="input-wrapp input-wrapp_horisontal">
                        <label for="province"></label>
                        <input id="province" value="" placeholder="Dropdown - Province / State" type="text">
                    </div>
                    <div class="input-wrapp input-wrapp_horisontal">
                        <label for="experience">Work experience</label>
                        <input id="experience" value="" placeholder="Work experience" type="text">
                    </div>
                    <div class="input-wrapp input-wrapp_horisontal">
                        <label for="years">Years experience</label>
                        <input id="years" value="" placeholder="Dropdown - Province / State" type="text">
                    </div>
                    <div class="input-wrapp input-wrapp_horisontal">
                        <label for="core">Core offering</label>
                        <input id="core" value="" placeholder="Dropdown A" type="text">
                    </div>
                    <div class="input-wrapp input-wrapp_horisontal">
                        <label for="skill">Skill category</label>
                        <input id="skill" value="" placeholder="Dropdown A" type="text">
                    </div>
                    <div class="input-wrapp input-wrapp_horisontal">
                        <label for="preferred">Preferred skill</label>
                        <input id="preferred" value="" placeholder="Dropdown B" type="text">
                    </div>
                    <div class="input-wrapp input-wrapp_horisontal">
                        <label for="location">Preferred location</label>
                        <input id="location" value="" placeholder="Dropdown A" type="text">
                    </div>
                </form>
                <a href="" class="form-btn"><span class="form-btn__text">NEXT</span></a>
                </div>
                <div class="steps-group">
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle circle_head circle_head_first active">1</div>
                        </div>
                        <div class="text text_head current-head">Signup to join</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle active"></div>
                        </div>
                        <div class="text current-step active">Personal information</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle active"></div>
                        </div>
                        <div class="text active">Verify your email</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle circle_head active">2</div>
                        </div>
                        <div class="text text_head active">Become an UberSkiller</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle active"></div>
                        </div>
                        <div class="text active">Select membership</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle active"></div>
                        </div>
                        <div class="text active">Membership payment</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle circle_head active">3</div>
                        </div>
                        <div class="text text_head active">Create your profile</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle"></div>
                        </div>
                        <div class="text">Member information</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle circle_head">4</div>
                        </div>
                        <div class="text text_head">Connect</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle"></div>
                        </div>
                        <div class="text">Invite friends</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle"></div>
                        </div>
                        <div class="text">Download app</div>
                    </div>
                </div>
            </div>
            </div>

        </section>

        @include('app.layout.footer')
    </section>
@endsection