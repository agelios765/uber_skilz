@extends('app.layout.app')

@section('content')
    <section class="wrapper wrapper_sign-up">
        @include('app.layout.header')
        @include('app.layout.mobile')
        <section class="form-area">
            <h2 class="form-area__title">Get connected</h2>
            <h2 class="form-area__title_big">Create your profile</h2>

            <div class="form-area__form form-area__form_top">
                <div class="form-group">
                    <span class="form-area__text">Please complete all the fields.</span>
                    <h2 class="form-area__title form-area__title_underline">Business (SME)</h2>
                    <form method="post" action="{{ route('user.business_sector') }}">
                        @csrf
                        <div class="input-wrapp input-wrapp_horisontal no-margin">
                            <label for="town">Location</label>
                            <input class="no-border-bottom" id="town" name="country" value="" placeholder="Dropdown - Town / City"
                                   type="text">
                        </div>
                        <div class="input-wrapp input-wrapp_horisontal">
                            <label for="province"></label>
                            <input id="province" value="" name="city" placeholder="Dropdown - Province / State" type="text">
                        </div>
                        <div class="input-wrapp input-wrapp_horisontal">
                            <label for="site">Website address</label>
                            <input id="site" value="" name="website" placeholder="" type="text">
                        </div>
                        <div class="input-wrapp input-wrapp_horisontal">
                            <label for="industry">Industry sector</label>
                            <select id="industry" name="industry">
                                <option value="">Select industry</option>
                                @foreach($data as $k => $item)
                                    <option value="{{ $k }}">{{ $item }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input-wrapp input-wrapp_horisontal">
                            <label for="business">Type of business</label>
                            <select id="business" name="business">
                                <option value="">Select</option>
                            </select>
                        </div>
                        <div class="input-wrapp input-wrapp_horisontal">
                            <label for="characters">Core skillz</label>
                            <input id="characters" value="" name="skillz" placeholder="In your own words; less than 80 characters"
                                   type="text">
                        </div>
                        <button type="submit" class="form-btn"><span class="form-btn__text">NEXT</span></button>
                        <a href="{{ route('user.connect') }}" class="form-btn"><span class="form-btn__text">BACK</span></a>
                    </form>
                </div>
                <div class="steps-group">
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle circle_head circle_head_first active">1</div>
                        </div>
                        <div class="text text_head current-head">Signup to join</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle active"></div>
                        </div>
                        <div class="text current-step active">Personal information</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle active"></div>
                        </div>
                        <div class="text active">Verify your email</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle circle_head active">2</div>
                        </div>
                        <div class="text text_head active">Become an UberSkiller</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle active"></div>
                        </div>
                        <div class="text active">Select membership</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle active"></div>
                        </div>
                        <div class="text active">Member information</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle"></div>
                        </div>
                        <div class="text">Membership payment</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle circle_head">3</div>
                        </div>
                        <div class="text text_head">Connect</div>
                    </div>
                    <div class="steps-group__item">
                        <div class="circle-wrapp">
                            <div class="circle"></div>
                        </div>
                        <div class="text">Download app</div>
                    </div>
                </div>

            </div>
        </section>
        @include('app.layout.footer')
    </section>
@endsection

@section('script')
    <script>
        $('#industry').change(function () {
            $.ajax({
                type: "GET",
                url: "{{ route('user.getSubcategory') }}",
                data: "val=" + $(this).val(),
                async: false,
                success: function (response) {
                    var $dropdown = $("#business");
                    $dropdown.find('option')
                        .remove()
                        .end();
                    $.each(response, function (k, v) {
                        $dropdown.append($("<option />").val(k).text(v));
                    });
                }
            });
        });
    </script>
@endsection