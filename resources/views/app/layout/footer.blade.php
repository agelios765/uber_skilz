<footer class="footer">
    <div class="footer__inner">
        <div class="footer__top">
            <div class="footer__top__item">
                <a class="link" href="mailto:info@uberskillz.com">info@uberskillz.com</a>
            </div>
            <div class="footer__top__item">
                <ul class="about-list">
                    <li class="about-list__item about-list__item_head">About UberSkillz</li>
                    <li class="about-list__item"><a href="{{ route('dashboard') }}" class="nlink">Home</a></li>
                    <li class="about-list__item"><a href="{{ route('purpose') }}" class="nlink">Purpose</a></li>
                    <li class="about-list__item"><a href="{{ route('benefits') }}" class="nlink">Benefits</a></li>
                    <li class="about-list__item"><a href="{{ route('getConnected') }}" class="nlink">Get Connected</a></li>
                </ul>
            </div>
            <div class="footer__top__item">
                <ul class="more-list">
                    <li class="more-list__item more-list__item_head">Know more</li>
                    <li class="more-list__item"><a href="{{ route('getConnected') }}#faq" class="nlink">FAQs</a></li>
                    <li class="more-list__item"><a href="{{ route('getConnected') }}#links" class="nlink">Links</a></li>
                    <li class="more-list__item"><a href="" class="nlink">UberSkillz App</a></li>
                    <li class="more-list__item"><a href="" class="nlink">SkillzFinder App</a></li>
                </ul>
            </div>
        </div>
        <div class="footer__bottom">
            <div class="footer__bottom__item">
                <span class="copy">© 2018 UberSkillz (Pty) Ltd</span>
            </div>
            <div class="footer__bottom__item">
                <a href="{{ route('privacy') }}" class="privacy">Privacy</a>
                <a href="{{ route('terms') }}" class="terms">Terms</a>
            </div>
        </div>
    </div>
</footer>