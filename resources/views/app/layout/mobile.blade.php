<section class="mobile-nav js-mobile-menu">
    <ul class="mobile-nav__list js-list">
        <li class="mobile-nav__item"><a href="{{ route('dashboard') }}" class="link">Home</a></li>
        <li class="mobile-nav__item"><a href="{{ route('purpose') }}" class="link">Purpose</a></li>
        <li class="mobile-nav__item"><a href="{{ route('benefits') }}" class="link">Benefits</a></li>
        <li class="mobile-nav__item"><a href="{{ route('getConnected') }}" class="link">How it work</a></li>
        <li class="mobile-nav__item"><a href="{{ route('getConnected') }}" class="link">Get Connected</a></li>
    </ul>
    <div class="mobile-nav__close js-close"></div>
</section>