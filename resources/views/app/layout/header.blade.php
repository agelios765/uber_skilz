<header class="header">
    <div class="header__inner">
        <div class="logo-area">
            <a href="{{ route('dashboard') }}" class="logo">
                <img src="{{ asset('images/main/logo.png') }}" alt="">
            </a>
        </div>
        <nav class="nav">
            <ul class="nav__list js-list-to-hide">
                <li class="nav__list__item"><a class="link" href="{{ route('sign_up') }}">Sign Up</a></li>
                <li class="nav__list__item"><a class="link" href="{{ route('login') }}">Log In</a></li>
                <li class="nav__list__item nav__list__item_icon"></li>
                <li class="nav__list__item nav__list__item_mobile-nav js-mobile-nav-btn">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </li>
            </ul>
        </nav>
    </div>
</header>
