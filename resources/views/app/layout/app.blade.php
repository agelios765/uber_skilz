<!DOCTYPE html>
<html lang="en">
<head>
    @include('app.layout.head')
</head>
<body>
@yield('content')

<script type='text/javascript' src={{ asset('js/particles-js/particles.js') }}></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script src="{{ asset('js/source/function.js') }}"></script>
@yield('script')
</body>
</html>