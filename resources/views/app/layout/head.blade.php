<meta charset="UTF-8">
<title>Skillz – Connecting Skills with Opportunities</title>
<link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">
<script src="{{ asset('js/jquery-1.8.1.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('css/style.min.css') }}">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link rel='stylesheet'
      id='vc_google_fonts_roboto100100italic300300italicregularitalic500500italic700700italic900900italic-css'
      href='http://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C300%2C300italic%2Cregular%2Citalic%2C500%2C500italic%2C700%2C700italic%2C900%2C900italic&#038;ver=4.9.8'
      type='text/css' media='all'/>
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">