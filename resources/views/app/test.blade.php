<section class="form-area">
    <div class="form-area__form">
        <h2 class="form-area__title">Get connected</h2>
        <h2 class="form-area__title_big">Verify your email</h2>
        <span class="form-area__text">Please verify your email address to activate your membership. </span>
        <span class="form-area__text">By clicking ‘Verify Email’, I hereby agree to the <a href="privacy.html"
                                                                                           target="_blank"
                                                                                           class="form-area__text__bold">Privacy</a> and <a
                    href="terms.html" target="_blank" class="form-area__text__bold">Terms</a> of
the UberSkillz service and will abide to its principles.</span>
        <a href="" class="form-btn"><span class="form-btn__text">VERIFY EMAIL</span></a>
    </div>
</section>



<section class="form-area">
    <div class="form-area__form">
        <h2 class="form-area__title">Get connected</h2>
        <h2 class="form-area__title_big">Resend email</h2>
        <span class="form-area__text">Please verify the email we just sent to your email address.
Check your Spam folder or for any type errors if you can’t find it.</span>
        <div class="input-wrapp input-wrapp_small">
            <label for="email">Email</label>
            <input id="email" value="" type="email">
        </div>
        <a href="" class="form-btn-link">Resend email</a>
    </div>
</section>


<section class="form-area form-area_log-in">
    <img width="517" height="163" src="images/forms/ubrs.png" alt="">
    <h2 class="form-area__title">Sign In</h2>
    <form method="POST" action="send.php">
        <div class="input-wrapp">
            <label for="email">Email</label>
            <input id="email" value="" type="text">
        </div>
        <div class="input-wrapp">
            <label for="password">Password</label>
            <input id="password" value="" type="password">
        </div>
        <a href="" class="form-btn"><span class="form-btn__text">SIGN IN</span></a>
    </form>
</section>

<section class="form-area">
    <div class="form-area__form">
        <h2 class="form-area__title">Get connected</h2>
        <h2 class="form-area__title_big">Become an Uberskiller</h2>
        <span class="form-area__text">Please select your type of membership required. </span>
        <form method="POST" action="send.php">
            <div class="radiobtn-area">
                <div class="radiobtn-area__text">Membership</div>
                <div class="radiobtn-area__items">
                    <div class="radiobtn-area__item">
                        <input type="radio" id="privateSector" name="gender" value="">
                        <label for="privateSector"><span>Individual – Private Sector</span></label>
                    </div>
                    <div class="radiobtn-area__item">
                        <input type="radio" id="government" name="gender" value="">
                        <label for="government"><span>Individual – Government Sector</span></label>
                    </div>
                    <div class="radiobtn-area__item">
                        <input type="radio" id="sme" name="gender" value="">
                        <label for="sme"><span>SME (Small Medium Enterprise) Company</span></label>
                    </div>
                </div>
            </div>
            <div class="input-wrapp">
                <label for="idNumber">First 8 digits of ID number</label>
                <input id="idNumber" value="" type="text">
            </div>
            <div class="input-wrapp">
                <label for="companyName">Company name</label>
                <input id="companyName" value="" type="text">
            </div>
            <div class="input-wrapp">
                <label for="companyId">Company registration number</label>
                <input id="companyId" value="" type="text">
            </div>
            <a href="" class="form-btn"><span class="form-btn__text">NEXT</span></a>
        </form>
    </div>
</section>

<section class="form-area">
    <div class="form-area__form">
        <h2 class="form-area__title">Get connected</h2>
        <h2 class="form-area__title_big">Membership Payment</h2>
        <span class="form-area__text">Super cheap annual membership!</span>
        <span class="form-area__text">R100 – Once-off admin fee</span>
        <span class="form-area__text">R500 – Membership Profile published in one skill category</span>
        <span class="form-area__text">R350 – 12-month Membership fee</span>
        <span class="form-area__text__big">Total: R 950</span>
        <a href="" class="form-btn"><span class="form-btn__text">PAY NOW </span></a>

        <span class="form-area__text"><span class="form-area__text__bold">Please note:</span>  Earn your R350 Membership fee back by successfully
introducing 2 or more friends; discount rates apply if you
would like to publish in more than one skill category – email us
your preferences for a fee estimation.</span>
    </div>
</section>
<section class="form-area">
    <h2 class="form-area__title">Get connected</h2>
    <h2 class="form-area__title_big">Create your profile</h2>
    <span class="form-area__text">Please complete all the required fields.</span>
    <h2 class="form-area__title form-area__title_inderline">Get connected</h2>
    <form method="POST" action="send.php">
        <div class="radiobtn-area">
            <div class="radiobtn-area__text">Education</div>
            <div class="radiobtn-area__items">
                <div class="radiobtn-area__item">
                    <input type="radio" id="secondary" name="Secondary" value="">
                    <label for="secondary"><span>Secondary</span></label>
                </div>
                <div class="radiobtn-area__item">
                    <input type="radio" id="tertiary" name="tertiary" value="">
                    <label for="tertiary"><span>Tertiary</span></label>
                </div>
            </div>
        </div>

        <div class="input-wrapp input-wrapp_horisontal no-margin">
            <label for="town">Location</label>
            <input class="no-border-bottom" id="town" value="" placeholder="Dropdown - Town / City" type="text">
        </div>
        <div class="input-wrapp input-wrapp_horisontal">
            <label for="province"></label>
            <input id="province" value="" placeholder="Dropdown - Province / State" type="text">
        </div>
        <div class="input-wrapp input-wrapp_horisontal">
            <label for="experience">Work experience</label>
            <input id="experience" value="" placeholder="Work experience" type="text">
        </div>
        <div class="input-wrapp input-wrapp_horisontal">
            <label for="years">Years experience</label>
            <input id="years" value="" placeholder="Dropdown - Province / State" type="text">
        </div>
        <div class="input-wrapp input-wrapp_horisontal">
            <label for="core">Core offering</label>
            <input id="core" value="" placeholder="Dropdown A" type="text">
        </div>
        <div class="input-wrapp input-wrapp_horisontal">
            <label for="skill">Skill category</label>
            <input id="skill" value="" placeholder="Dropdown A" type="text">
        </div>
        <div class="input-wrapp input-wrapp_horisontal">
            <label for="preferred">Preferred skill</label>
            <input id="preferred" value="" placeholder="Dropdown B" type="text">
        </div>
        <div class="input-wrapp input-wrapp_horisontal">
            <label for="location">Preferred location</label>
            <input id="location" value="" placeholder="Dropdown A" type="text">
        </div>
    </form>
    <a href="" class="form-btn"><span class="form-btn__text">NEXT</span></a>
</section>