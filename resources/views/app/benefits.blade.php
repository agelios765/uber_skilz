@extends('app.layout.app')

@section('content')
    <section class="wrapper wrapper_benefits">
        @include('app.layout.header')
        @include('app.layout.mobile')
        <section class="benefits">
            <div class="benefits__inner">
                <div class="benefits__item">
                    <div class="benefits__circle">
                        <div class="circle"></div>
                    </div>
                    <div class="benefits__desc">
                        <h2 class="benefits__desc__title">Benefits for</h2>
                        <h3 class="benefits__desc__title-big">All South Africans</h3>
                        <ul class="benefits__list">
                            <li class="benefits__list__item">UberSkillz is a direct response to South Africa’s NEW DAWN notion which comprises key changes in social and economic relief, recovery and reform.</li>
                            <li class="benefits__list__item">It is key to the recovery of the South African economy.</li>
                            <li class="benefits__list__item">Promotes unity of purpose and is committed to bring about a positive shift in our country’s future economic circumstances and destiny.</li>
                            <li class="benefits__list__item">Creates work opportunities for all South Africans, irrespective of age, race, creed or gender.</li>
                            <li class="benefits__list__item">Promotes the principles of good work ethics, responsibility, accountability, integrity, honesty, respect and punctuality and aims to attract like-minded people to its platform.</li>
                            <li class="benefits__list__item">Actively promotes and advances Grey Power and invites membership from this important skills base.</li>
                            <li class="benefits__list__item">Well positioned to provide expertise and experience to build skill capacity in the public and private sectors.</li>
                            <li class="benefits__list__item">Offers easy-to-use geolocation apps to find skills effortlessly.</li>
                            <li class="benefits__list__item">Aims to become the largest reputable skills aggregator in the world.</li>
                        </ul>
                    </div>
                </div>
                <div class="benefits__item">
                    <div class="benefits__circle">
                        <div class="circle"></div>
                    </div>
                    <div class="benefits__desc">
                        <h2 class="benefits__desc__title">Benefits for</h2>
                        <h3 class="benefits__desc__title-big">UberSkillz members</h3>
                        <ul class="benefits__list">
                            <li class="benefits__list__item">Sell your skills by making them visible to others.</li>
                            <li class="benefits__list__item">Free UberSkillz App – cool geolocation connectivity for Android and IOS.</li>
                            <li class="benefits__list__item">Easy find-me function.</li>
                            <li class="benefits__list__item">Boost your profile for better reach.</li>
                            <li class="benefits__list__item">Create your own easy-to-use, unique profile.</li>
                            <li class="benefits__list__item">Organised and orderly skills category platform.</li>
                            <li class="benefits__list__item">Work, provide a service, consult, mentor, transfer skills.</li>
                            <li class="benefits__list__item">Be a part of the energy of your community.</li>
                            <li class="benefits__list__item">Be part of an incredible success story.</li>
                            <li class="benefits__list__item">Super cheap annual membership.</li>
                            <li class="benefits__list__item">Download cool business tools; live chat; CV and media uploads; helpdesk; groups and forum capability.</li>
                        </ul>
                    </div>
                </div>
                <div class="benefits__item">
                    <div class="benefits__circle">
                        <div class="circle"></div>
                    </div>
                    <div class="benefits__desc">
                        <h2 class="benefits__desc__title">Benefits for</h2>
                        <h3 class="benefits__desc__title-big">UberSkillz users</h3>
                        <ul class="benefits__list">
                            <li class="benefits__list__item">Find skills instantly – from a 5 km radius to worldwide.</li>
                            <li class="benefits__list__item">Free SkillzFinder App and usage.</li>
                            <li class="benefits__list__item">Easy to use and to navigate.</li>
                            <li class="benefits__list__item">Chat capability between yourself and the UbersSkiller.</li>
                            <li class="benefits__list__item">Service provider finds you.</li>
                            <li class="benefits__list__item">Service rating capability for the UberSkiller’s service.</li>
                            <li class="benefits__list__item">NO advertisements or pop-ups.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="benefits-desc">
            <div class="benefits-desc__inner">
                <span class="benefits-desc__text benefits-desc__text_first">It is easy to see why UberSkillz is a must for everyone who requires work or who wants to make a change, or for SMME service providers and owners to advertise their services in their immediate surroundings.</span>
                <span class="benefits-desc__text">UberSkillz delivers its service on any device, in context, at your moment of need.</span>
            </div>
        </section>
        @include('app.layout.footer')
    </section>

@endsection