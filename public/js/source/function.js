$( document ).ready(function() {
    var drop = $('.drop');
    var dropBtn = $('.js-drop');
    var defaultHeight = 48;
    if(screen.width < 960){
        defaultHeight = 96
    }
    dropBtn.click(function () {
        var endHeight = $(this).next('.drop__bottom').outerHeight();
        if ($(this).hasClass('opened')){
            $(this).closest(drop).animate({
                height: defaultHeight
            });
        }
        else {
            $(this).closest(drop).animate({
                height: defaultHeight + endHeight
            });
        }
        $(this).toggleClass('opened').find('.js-arrow').toggleClass('down');

    });
    var mobileMenu = $('.js-mobile-menu');
    var mobileList = $('.js-list');
    var mobileHeadList = $('.js-list-to-hide');
    var mobileMenuBtn = $('.js-mobile-nav-btn');
    var closeBtn = $('.js-close');
    mobileMenuBtn.click(function () {
        mobileMenu.addClass('opened');
        setTimeout(function () {
            mobileList.addClass('opened');
            mobileHeadList.hide();
        });
    });
    closeBtn.click(function () {
        mobileList.removeClass('opened');
        setTimeout(function () {
            mobileMenu.removeClass('opened');
            mobileHeadList.show();
        }, 300)

    });

    $(".js-link").click(function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 500);
    });
    particlesJS("gp_particles_1", {
        "particles": {
            "number": {
                "value": 80,
                "density": {
                    "enable": true,
                    "value_area": 800
                }
            },
            "color": {
                "value": "#ffffff"
            },
            "shape": {
                "type": "circle",
                "stroke": {
                    "width": 0,
                    "color": "#000000"
                },
                "polygon": {
                    "nb_sides": 5
                },
                "image": {
                    "src": "img/github.svg",
                    "width": 100,
                    "height": 100
                }
            },
            "opacity": {
                "value": 0.5,
                "random": false,
                "anim": {
                    "enable": false,
                    "speed": 1,
                    "opacity_min": 0.1,
                    "sync": false
                }
            },
            "size": {
                "value": 4,
                "random": true,
                "anim": {
                    "enable": false,
                    "speed": 40,
                    "size_min": 0.5,
                    "sync": false
                }
            },
            "line_linked": {
                "enable": true,
                "distance": 150,
                "color": "#ffffff",
                "opacity": 0.4,
                "width": 1
            },
            "move": {
                "enable": true,
                "speed": 6,
                "direction": "none",
                "random": false,
                "straight": false,
                "out_mode": "out",
                "bounce": false,
                "attract": {
                    "enable": false,
                    "rotateX": 600,
                    "rotateY": 1200
                }
            }
        },
        "interactivity": {
            "detect_on": "canvas",
            "events": {
                "onhover": {
                    "enable": true,
                    "mode": "grab"
                },
                "onclick": {
                    "enable": true,
                    "mode": "push"
                },
                "resize": true
            },
            "modes": {
                "grab": {
                    "distance": 140,
                    "line_linked": {
                        "opacity": 1
                    }
                },
                "bubble": {
                    "distance": 400,
                    "size": 40,
                    "duration": 2,
                    "opacity": 8,
                    "speed": 3
                },
                "repulse": {
                    "distance": 200,
                    "duration": 0.4
                },
                "push": {
                    "particles_nb": 4
                },
                "remove": {
                    "particles_nb": 2
                }
            }
        },
        "retina_detect": true
    });
});
