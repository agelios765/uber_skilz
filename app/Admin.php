<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    protected $guard = 'admin';

    protected $guarded = ['id'];

    public function getRememberTokenName()
    {
        return null;
    }
}
