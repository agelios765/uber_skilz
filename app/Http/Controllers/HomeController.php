<?php

namespace App\Http\Controllers;


class HomeController extends Controller
{
    /**
     * Dashboard page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('app.index');
    }
}
