<?php

namespace App\Http\Controllers;

class PrivacyController extends Controller
{
    /**
     * Privacy page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('app.privacy');
    }
}
