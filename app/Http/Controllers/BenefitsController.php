<?php

namespace App\Http\Controllers;

class BenefitsController extends Controller
{
    /**
     * Benefits page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('app.benefits');
    }
}
