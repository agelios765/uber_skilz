<?php

namespace App\Http\Controllers;

class GetConnectedController extends Controller
{
    /**
     * GetConnected page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('app.getConnected');
    }
}
