<?php

namespace App\Http\Controllers;

class TermsController extends Controller
{
    /**
     * Terms page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('app.terms');
    }
}
