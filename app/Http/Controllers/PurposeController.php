<?php

namespace App\Http\Controllers;

class PurposeController extends Controller
{
    /**
     * Purpose page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('app.purpose');
    }
}
