<?php

namespace App\Http\Controllers;

use App\Order;
use Billow\Contracts\PaymentProcessor;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function profile()
    {
        return view('app.user.profile');
    }

    public function connect()
    {
        return view('app.user.connect');
    }

    public function applyConnect(Request $request)
    {
        $this->validate($request, [
            'gender' => 'required|in:1,2,3',
        ]);

        switch ($request->gender) {
            case 1:
                $route = 'user.private_sector';
                break;
            case 2:
                $route = 'user.goverment_sector';
                break;
            case 3:
                $route = 'user.business_sector';
                break;
            default:
                return redirect()->back();
        }

        auth()->user()->update([
            'membership' => $request->gender,
        ]);

        return redirect()->route($route);
    }

    public function getSubcategory(Request $request)
    {
        $data = [
            [
                'Crop farming',
                'foresters/timber traders',
                'paper manufacture',
                'fishermen/ fish farmers',
                'game farming',
                'livestock farming',
                'operators',
                'conservationists',
                'ecologists',
                'zoologists',
                'maritime research',
                'admin'
            ],
            [
                'Painters', 'musicians', 'sculptors', 'teachers', 'coaches', 'sound engineers', 'jewellers', 'DJ’s', 'dancers', 'actors', 'illustrators', 'poets', 'lyricists', 'writers', 'potters', 'proof readers', 'screen printers', 'glass blowers', 'admin'
            ],
            [
                'Pilots', 'mechanics', 'aero engineers', 'ATC’s', 'steward/ess', 'maintenance', 'airport services', 'managers', 'admin'
            ],
            [
                'Manufacture', 'wholesale', 'retail', 'managers', 'engineers', 'chemists', 'admin'
            ],
            [
                'Engineers', 'architects', 'draughtsmen', 'town planning', 'QS', 'project managers', 'artisans', 'equipment operators', 'inspectors', 'riggers', 'model builders', 'rebar workers', 'interior decorators', 'site managers', 'admin'
            ],
            [
                'Nails', 'hair', 'make-up artists', 'barbers', 'cosmetologists', 'stylists', 'admin'
            ],
            [
                'Coaches', 'teachers', 'adjudicators', 'examiners', 'admin'
            ],
            [
                'Engineers', 'electricians', 'manufacturing', 'wholesale', 'retail', 'admin'
            ],
            [
                'Electric', 'fossil fuels', 'renewables (solar, wind, hydro)', 'engineers', 'traders', 'maintenance', 'admin'
            ],
            [
                'Designers', 'manufacturing', 'wholesale', 'retail', 'managers', 'admin'
            ],
            [
                'Brokers', 'advisors', 'CA’s', 'bookkeepers', 'tax consultants', 'insurance', 'banking', 'investments', 'managers', 'admin'
            ],
            [
                'Wholesale', 'retail', 'managers', 'manufacturing', 'quality control', 'admin'
            ],
            [
                'Landscapers', 'garden services', 'growers', 'botanists', 'wholesale', 'retail', 'admin'
            ],
            [
                'H&S officers', 'security guards', 'nurses', 'audiologists', 'doctors', 'chemists', 'social workers', 'drug councillors', 'compliance officers', 'dermatologists', 'dental hygienists', 'healthcare workers', 'investigators', 'admin'
            ],
            [
                'Cleaning', 'animal minders', 'house sitters', 'tailors', 'dress makers'
            ],
            [
                'Catering services', 'chefs', 'cooks', 'managers', 'dealers', 'admin'
            ],
            [
                'IT specialists', 'web design', 'social media specialist', 'multimedia', 'publishing', 'journalists', 'broadcasters', 'translators', 'gaming', 'event services', 'marketing', 'advertising', 'logistics', 'admin'
            ],
            [
                'Lawyers', 'notaries', 'judges', 'arbitrators', 'mediators', 'hearing officers', 'secretaries', 'debt councillors', 'investigators', 'loss preventers', 'admin'
            ],
            [
                'Doctors', 'nurses', 'first aid', 'ambulance drivers', 'managers', 'hospital staff', 'research', 'dentists', 'dental technicians', 'pharmaceuticals', 'chemists', 'managers', 'rehabs and staff', 'social workers', 'counsellors', 'sex workers', 'admin'
            ],
            [
                'Geologists', 'prospectors', 'equipment operators', 'management', 'miners', 'admin'
            ],
            [
                'Engineers', 'plumbers', 'maintenance', 'managers', 'operators', 'admin'
            ],
            [
                'Organisations', 'foundations', 'admin'
            ],
            [
                'Photographers', 'models', 'film crew', 'videographers', 'set designers', 'admin'
            ],
            [
                'Estate agents', 'repair and maintenance', 'managers', 'admin'
            ],
            [
                'Leaders', 'admin', 'managers'
            ],
            [
                'Futurologists', 'rocket scientists', 'astronomers', 'numerologists'
            ],
            [
                'Trainers', 'life guards', 'team builders', 'referees', 'umpires', 'admin'
            ],
            [
                'Manufacturers', 'traders', 'import', 'export', 'admin'
            ],
            [
                'Drivers', 'mechanics', 'maintenance', 'logistics', 'automotive', 'designers', 'mechanical engineers', 'wholesale', 'retail', 'admin'
            ],
            [
                'Agents', 'game drivers', 'admin'
            ],
            [
                'Managers', 'sales', 'controllers', 'admin'
            ]
        ];

        return $data[$request->val];
    }

    public function privateSector()
    {
        $data = [
            'Agriculture, forestry, fishing and game',
            'Art, crafts and entertainment',
            'Aviation',
            'Chemical and Pharmaceuticals',
            'Construction',
            'Cosmetics/ personal',
            'Education',
            'Electrical',
            'Energy',
            'Fashion, Fabrics and Apparel',
            'Financial services',
            'Food and Beverage',
            'Gardening',
            'Health, safety and security',
            'Home services',
            'Hotel, restaurant and casino',
            'Information and communication',
            'Legal',
            'Medical and Social services',
            'Mining and Minerals',
            'Water, waste management, sanitation, environment',
            'Non-profit',
            'Photography and motion picture',
            'Property',
            'Religious',
            'Specialists',
            'Sport and recreation',
            'Trade and industry',
            'Transport and storage',
            'Travel, Tourism and Leisure',
            'Wholesale and retail'
        ];

        return view('app.user.private_sector', [
            'data' => $data
        ]);
    }

    public function storePrivateSector(Request $request)
    {
        $this->validate($request, [
            'skillz' => 'nullable|max:80',
            'sector' => 'required|in:1,2',
            'country' => 'required',
            'city' => 'required',
            'birthday' => 'required',
            'experience' => 'required|numeric',
            'department' => 'required',
            'occupational' => 'required'
        ]);

        auth()->user()->update([
            'skillz' => $request->skillz,
            'education' => $request->sector,
            'country' => $request->country,
            'city' => $request->city,
            'dateOfBirth' => $request->birthday,
            'years_experience' => $request->experience,
            'industry_sector' => $request->department,
            'sub_sector' => $request->occupational
        ]);

        return redirect()->route('user.member');
    }

    public function govermentSector()
    {
        $departments = [
            'Agriculture, forestry and fishing',
            'Arts and Culture',
            'Communications',
            'Co-operative governance',
            'Correctional Services',
            'Defence',
            'Education',
            'Energy',
            'Environmental affairs',
            'GCIS',
            'Health',
            'Home affairs',
            'Human Settlement',
            'ICD',
            'Justice',
            'Foreign Affairs',
            'Labour',
            'Mineral Resources',
            'National Intelligence',
            'Treasury',
            'Police',
            'PSA',
            'Public Works',
            'Rural Development',
            'Science and Technology',
            'Social Development',
            'SARS',
            'SASS',
            'Sport and recreation',
            'Statistics',
            'Tourism'
        ];

        $occupational = [
            'Junior management',
            'Mid management',
            'Senior management',
            'Executive/top management'
        ];

        return view('app.user.goverment_sector', compact('departments', 'occupational'));
    }

    public function storeGovermentSector(Request $request)
    {
        $this->validate($request, [
            'sector' => 'required|in:1,2,3,4',
            'country' => 'required',
            'city' => 'required',
            'birthday' => 'required',
            'experience' => 'required|numeric',
            'department' => 'required',
            'occupation' => 'required'
        ]);

        auth()->user()->update([
            'goverment' => $request->sector,
            'country' => $request->country,
            'city' => $request->city,
            'dateOfBirth' => $request->birthday,
            'years_experience' => $request->experience,
            'department' => $request->department,
            'occupation' => $request->occupational
        ]);

        return redirect()->route('user.member');
    }

    public function businessSector()
    {
        $data = [
            'Agriculture, forestry, fishing and game',
            'Art, crafts and entertainment',
            'Aviation',
            'Chemical and Pharmaceuticals',
            'Construction',
            'Cosmetics/ personal',
            'Education',
            'Electrical',
            'Energy',
            'Fashion, Fabrics and Apparel',
            'Financial services',
            'Food and Beverage',
            'Gardening',
            'Health, safety and security',
            'Home services',
            'Hotel, restaurant and casino',
            'Information and communication',
            'Legal',
            'Medical and Social services',
            'Mining and Minerals',
            'Water, waste management, sanitation, environment',
            'Non-profit',
            'Photography and motion picture',
            'Property',
            'Religious',
            'Specialists',
            'Sport and recreation',
            'Trade and industry',
            'Transport and storage',
            'Travel, Tourism and Leisure',
            'Wholesale and retail'
        ];
        return view('app.user.business_sector', [
            'data' => $data
        ]);
    }

    public function storeBusinessSector(Request $request)
    {
        $this->validate($request, [
            'country' => 'required',
            'city' => 'required',
            'website' => 'required',
            'industry' => 'required',
            'business' => 'required',
            'skillz' => 'required'
        ]);

        auth()->user()->update([
            'country' => $request->country,
            'city' => $request->city,
            'industry_sector' => $request->industry,
            'sub_sector' => $request->business,
            'skillz' => $request->skillz,
            'website' => $request->website,
        ]);

        return redirect()->route('user.member');

    }

    public function invite()
    {
        return view('app.user.invite');
    }

    public function download()
    {
        return view('app.user.download');
    }

    public function member(PaymentProcessor $payfast)
    {
//        $cartTotal = 9999;
        $order = Order::create([
            'payment_id' => uniqid(), // A unique reference for the order.
            'amount' => 95000,
            'user_id' => auth()->id(),
            'status' => 0
        ]);

        // Build up payment Paramaters.
        $payfast->setBuyer('first name', 'last name', 'email');
        $payfast->setAmount(95000);
        $payfast->setItem('item-title', 'item-description');
        $payfast->setMerchantReference('001');

        return view('app.user.member', [
            'form' => $payfast->paymentForm('PAY NOW')
        ]);
    }
}
