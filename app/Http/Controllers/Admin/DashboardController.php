<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        return view('admin.index', [
            'users' => User::where('email_verified_at', '!=', null)->count()
        ]);
    }
}
