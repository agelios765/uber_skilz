<?php

namespace App\Http\Controllers;

use App\Order;
use Billow\Contracts\PaymentProcessor;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function successful()
    {
        return view('app.payment.success');
    }

    public function cancel()
    {
        return view('app.payment.cancel');
    }

    public function itn(Request $request, PaymentProcessor $payfast)
    {
// Retrieve the Order from persistance. Eloquent Example.
        $order = Order::where('payment_id', $request->get('m_payment_id'))->firstOrFail(); // Eloquent Example

        // Verify the payment status.
        $status = $payfast->verify($request, $order->amount, $order->m_payment_id)->status();

        // Handle the result of the transaction.
        switch ($status) {
            case 'COMPLETE': // Things went as planned, update your order status and notify the customer/admins.
                $order->user->update([
                    'paid' => 1
                ]);
                break;
            case 'FAILED': // We've got problems, notify admin and contact Payfast Support.
                break;
            case 'PENDING': // We've got problems, notify admin and contact Payfast Support.
                break;
            default: // We've got problems, notify admin to check logs.
                break;
        }
    }
}
