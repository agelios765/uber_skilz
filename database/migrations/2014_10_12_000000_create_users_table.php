<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lastname');
            $table->string('number');

            $table->integer('paid')->default(0);

            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();

            $table->integer('membership')->nullable();
//            $table->integer('membership_id')->nullable();
//            $table->string('company_name')->nullable();
//            $table->string('company_regist_number')->nullable();

            $table->integer('education')->nullable();
            $table->integer('goverment')->nullable();

            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('dateOfBirth')->nullable();
//            $table->string('experience')->nullable();

            $table->integer('years_experience')->nullable();

            $table->integer('industry_sector')->nullable();
            $table->integer('sub_sector')->nullable();

            $table->integer('department')->nullable();
            $table->integer('occupation')->nullable();

            $table->string('website')->nullable();
            $table->string('skillz')->nullable();

//            $table->integer('preferred_skill')->nullable();
//            $table->integer('preferred_location')->nullable();

//            $table->integer('education')->nullable();

            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
