<?php

use Faker\Generator as Faker;

$factory->define(\App\Admin::class, function (Faker $faker) {
    return [
        'email' => $faker->email,
        'password' => \Illuminate\Support\Facades\Hash::make('secret')
    ];
});
